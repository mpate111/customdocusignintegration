({
    loadAttachments: function (component, event, helper) {
        // $A.get('e.force:refreshView').fire();
        console.log('load Attachments')
        $A.util.removeClass(component.find("Spinner"), "slds-hide");
        var action = component.get("c.loadAttachments");
        var agrId = component.get('v.propertyId');
        action.setParams({
            objectId: agrId
        });
        action.setCallback(this, function (a) {
          try {
            if (a.getState() === "SUCCESS") 
            {    
              if (a.getReturnValue().length > 0) 
              {
                var records = a.getReturnValue();
                records.forEach(function (record) {
                  record.isChecked = false;
                });
                console.log('records', records);
                component.set("v.records", records);
              }
            } 
            else if (a.getState() === "ERROR") 
            {
              $A.log("Errors", a.getError());
            }
          } catch (error) {
            console.error(error.stack);
          }
          $A.util.addClass(component.find("Spinner"), "slds-hide");
        });
        $A.enqueueAction(action);
      },

      selectFilesAndSubmit: function (component,event, helper) {
        $A.util.removeClass(component.find("Spinner"), "slds-hide");
        
        console.log('selectFilesAndSubmit')
        var files = [];
        var records = component.get('v.records');
        // helper.attachedfiles(component, helper, records);
        console.log(records);
        console.log('selectFilesAndSubmit')
        records.forEach(record => {
          if (record.isChecked) 
          {
            files.push(record);
            console.log('selectFilesAndSubmit')
          }
        //   Console.log("Maulik-Files"+files);
        });
        if (files.length != 0) 
        {
            // console.log(files);
            // console.log('Inside attachedfiles');
            var action = component.get("c.docuSignCallout");
            console.log('Inside attachedfiles');
            var propertyid = component.get('v.propertyId');
            action.setParams({
             selectedFiles : files,
             propertyId : propertyid   
            });
            action.setCallback(this, function(a) {
              console.log(a.getState());
              console.log(a.getReturnValue());
                try {
                    if(a.getState() === "SUCCESS")
                    {
                        if(a.getReturnValue() != null)
                        {
                            this.fireToast('success', 'Success!', 'Successfully send for DocuSign E-Signature','sticky');
                            // $A.util.addClass(component.find("Spinner"), "slds-hide");
                        }
                    }
                    else if(a.getState() === "ERROR")
                    {
                      this.fireToast('Error', 'Error', 'Unable to Send for DocuSign','sticky');
                    }
                    } catch (error) {
                      console.error(error.stack);
                    }
                $A.util.addClass(component.find("Spinner"), "slds-hide");
                this.return(component, event, helper);
            });
            $A.enqueueAction(action);
            // $A.util.addClass(component.find("Spinner"), "slds-hide");
        } 
        else 
        {
          this.fireToast('error', 'Error!', 'Select Files to Submit.','sticky')
          $A.util.addClass(component.find("Spinner"), "slds-hide");
        }
      },

      fireToast: function (type, title, message,mode) {
        var toastEvent = $A.get('e.force:showToast');
        toastEvent.setParams({
          'title': title,
          'type': type,
          'mode': mode,
          'message': message
        });
        toastEvent.fire();
      },
      return : function(component){
        $A.get('e.force:refreshView').fire();
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
          "recordId": component.get('v.propertyId'),
          "slideDevName": "detail"
        });
        navEvt.fire();
      }
})
