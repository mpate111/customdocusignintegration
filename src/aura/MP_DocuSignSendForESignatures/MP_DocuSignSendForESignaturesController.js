({
   
    scriptsLoaded: function (component, event, helper) {
        var pageReference = component.get("v.pageReference");
        component.set("v.propertyId", pageReference.state.c__propertyId);
        component.set('v.propertyName', pageReference.state.c__propertyName);
        console.log('Maulik-PropertyId :'+pageReference.state.c__propertyId);
        console.log('Maulik-PropertyName :'+pageReference.state.c__propertyName);
        // $A.get('e.force:refreshView').fire();
        // window.location
         helper.loadAttachments(component, event, helper);
    },
    onSelectAllChange: function (component, event, helper) {
        var isAllSelected = component.get('v.isAllSelected');
        const checkboxes = component.get('v.records');
        if (checkboxes) 
        {
            let chk = (checkboxes.length == null) ? [checkboxes] : checkboxes;
            chk.forEach(record => record.isChecked = isAllSelected);
            component.set('v.records', chk);
        }
    },
    return : function(component, event, helper){
        helper.return(component, event, helper);
    },
    submit : function(component, event, helper){
        helper.selectFilesAndSubmit(component, helper);
    }
    
})
