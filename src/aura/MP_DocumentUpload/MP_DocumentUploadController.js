({

    handleUploadFinished: function (component, event, helper) {
    // show spinner by toggling class
    var spinner = component.find("mySpinner");
    $A.util.toggleClass(spinner, "slds-hide");
    // call component helper function
    helper.updateDate(component, event, helper);        
    }
})
