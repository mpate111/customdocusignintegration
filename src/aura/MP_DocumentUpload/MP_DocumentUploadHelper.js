({
    updateDate: function(component, event) {
        var uploadedFiles = event.getParam("files");
        var documentId = uploadedFiles[0].documentId;
        var fileName = uploadedFiles[0].name;
        
        console.log('documentId: ' + documentId);

        var uploadDate = component.get("v.inputDate");
        
        // invoke apex controller method
        var action = component.get("c.processDocument");
        action.setParams({
            PropertyId: component.get("v.recordId"),
            documentId: documentId,
            fileName: fileName,
            startdate : uploadDate
        });
        
        // callback function which receives the response from the server
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('state: ' + state);
            var toastEvent = $A.get("e.force:showToast");
            
            if (state === "SUCCESS") 
            {    
                var result = response.getReturnValue()
                console.log('MP** response'+result);
                
                var spinner = component.find("mySpinner");
        		$A.util.toggleClass(spinner, "slds-hide");
                // show success message and close File Upload Panel
                if(result.isSuccess)
                {  
                    this.fireToast('success', 'Success!', 'Document has been uploaded successfully!','sticky');                                      
                    $A.get('e.force:refreshView').fire();                    
                // show error message if server returned an error
                }
                else
                {
                    this.fireToast('error', 'Failure', 'Document couldn\'t be imported successfully due to this error: \n' + result.errorMsg,'sticky');
                }
            }
            else 
            {
                var errors = response.getError();
                // console.log('MP** errors'+errors);
                if (errors && Array.isArray(errors) && errors.length > 0) 
                {
                    console.log("MP** Error message: " + errors[0].message);                    
                    this.fireToast('error', 'Failure', 'Document couldn\'t be imported successfully due to this error: \n' + errors[0].message,'sticky');
                                       
                } else 
                {
                    console.log("Unknown error");
                }
            }
            // Close the action panel
            var dismissActionPanel = $A.get("e.force:closeQuickAction");
            dismissActionPanel.fire();
        });
        $A.enqueueAction(action);
		// }
    },
    fireToast: function (type, title, message,mode) {
        var toastEvent = $A.get('e.force:showToast');
        toastEvent.setParams({
          'type': type,
          'title': title,
          'message': message,
          'mode': mode
        });
        toastEvent.fire();
      }
})
