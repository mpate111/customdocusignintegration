/*************************************************************
@Name: MP_DocuSignController
@Author: Maulik Patel
@Description: Controller class for Lightning component Page MP_DocuSignSendForESignatures.
*******************************************************************/

public with sharing class MP_DocuSignController {
    
    @AuraEnabled
    public static List<MP_DocuSignControllerHelper.AttachmentWrapper> loadAttachments(Id objectId){
       return MP_DocuSignControllerHelper.loadAttachments(objectId);
    }

    @AuraEnabled
    public static Boolean docuSignCallout(List<MP_DocuSignControllerHelper.AttachmentWrapper> selectedFiles,String propertyId){
        return MP_DocuSignControllerHelper.docuSignCallout(selectedFiles, propertyId);
    }
}
