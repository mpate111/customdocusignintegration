/***************************************************************************************************************
@Name: MP_DocuSignEnvelopeCallout
@Author: Maulik Patel
@Description: Create docusign envelope using docuSign APIs and Insert record in DocusignEnvelope custom object
***************************************************************************************************************/

global with sharing class MP_DocuSignEnvelopeCallout implements Queueable,Database.AllowsCallouts {

    public static String docuSignAccountNumber;
    public static String docuSignAppId;
    public static String endPoint;

    List<MP_DocuSignControllerHelper.AttachmentWrapper> oAttachmentWrapperList = new List<MP_DocuSignControllerHelper.AttachmentWrapper>();
    String propertyId;

    public MP_DocuSignEnvelopeCallout(List<MP_DocuSignControllerHelper.AttachmentWrapper> inputData,String propertyID) {
        this.oAttachmentWrapperList = inputData;
        this.propertyId = propertyID;
    }

    global void execute(QueueableContext context) {
        String accessToken;
        String body;
        String email;
        System.debug('MP** Inside the Queueable class');
        
        email = [Select ID,Contact__r.email from Property__c where Id =: propertyId Limit 1].Contact__r.email;
        //Fetch the docuSignAccountNum and AppID from custom metadata field
        Docusign_Setting__mdt DocuSignAccountDetails = [select DocuSign_Account_Number__c
                                                            ,DocuSign_Application_ID__c 
                                                            from DocuSign_Setting__mdt where label = 'DocuSign Details' LIMIT 1];                  
        docuSignAccountNumber = DocuSignAccountDetails.DocuSign_Account_Number__c;
        docuSignAppId = DocuSignAccountDetails.DocuSign_Application_ID__c;

        System.debug('MP** : docuSignAccountNumber : '+docuSignAccountNumber+' docuSignAppId : '+docuSignAppId);
        accessToken = getAccessToken(docuSignAppId);
        body = JSONBody(oAttachmentWrapperList,email);
        createDocuSignEnvelope(docuSignAccountNumber,accessToken, body,propertyId) ;
    }

    /*
    @Description: Method fpr creating DocuSign Envelope
	@Author: Maulik Patel
	*/
    public static void createDocuSignEnvelope(String docuSignAccountNumber, String accessToken, String body,String propertyId){
        HttpResponse response = new HttpResponse();
        String EnvelopeId;
        String Status;
        String StatusDateTime;
        DocuSignEnvelopeResponseWrapper docuSignEnvelope;

        endPoint = 'callout:MP_DocuSignCredentials/accounts/'+docuSignAccountNumber+'/envelopes';
        // endPoint = 'https://demo.docusign.net/restapi/v2.1/accounts/'+docuSignAccountNumber+'/envelopes';

        try {
            response = doHttpCallout(endPoint, 'POST', 'application/json', accessToken, body);
        } catch (Exception e) {
            System.debug('MP** Inside the createDocuSignEnvelope method and something is wrong with the call out'+e.getStackTraceString());
        }
        if(response.getStatusCode() == 201){
            System.debug('Successfully created the Envelope Maulik ');
            docuSignEnvelope = (DocuSignEnvelopeResponseWrapper)JSON.deserialize(response.getBody(), DocuSignEnvelopeResponseWrapper.class);
            System.debug('MP** Deserialised docuSignEnvelope response'+docuSignEnvelope);
        }
        EnvelopeId = docuSignEnvelope.envelopeId;
        Status = String.valueOf(docuSignEnvelope.status);
        StatusDateTime = String.valueOf(docuSignEnvelope.statusDateTime);
        System.debug('MP** The output EnvelopeId : '+EnvelopeId+' Status : '+Status + ' StatusDateTime : '+StatusDateTime);

        DocuSign_Envelope__c oEnvelope = new DocuSign_Envelope__c();
        oEnvelope.Envelope_ID__c = EnvelopeId;
        oEnvelope.Status__c = Status;
        oEnvelope.property__c = propertyId;

        try {
            Insert oEnvelope;
        } catch (Exception e) {
            System.debug('MP** Unable to Insert the DocuSignEnvelope Record'+ e.getStackTraceString);
        }
    }
    

    /*
    @Description: For gettig the access token of Docusign
	@Author: Maulik Patel
	*/
    global static string getAccessToken(String docuSignAppId){
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('callout:MP_DocuSignCredentials/oauth2/token');
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        //request.setHeader('name', 'x-docusign-authentication');
        // Set the body as a JSON object
        string oBody = 'grant_type=password&client_id='+docuSignAppId+'&username={!HTMLENCODE($Credential.Username)}&password={!HTMLENCODE($Credential.Password)}&scope=api';
        request.setBody(oBody);
        System.debug('HttpRequest'+request.getBody());
        HttpResponse response = http.send(request);
        System.debug('HttpResponse '+response);
        String accessToken = '';
        
        // Parse the JSON response
       if (response.getStatusCode() == 200) 
       {
            System.debug(response.getBody());
            AccessTokenWrapper accessTokenWrap = (AccessTokenWrapper)JSON.deserialize(response.getBody(), AccessTokenWrapper.class);
            accessToken = accessTokenWrap.access_token;
            
        } 
        else 
        {
            System.debug('MP** HttpErrorRequest'+request);
            //HttpResponse response = http.send(request);
            System.debug('MP** HttpErrorResponse '+response);
            System.debug('MP** The status code returned was not expected: ' +response.getStatusCode() + ' ' + response.getStatus());
        }
        System.debug('MP**  accessToken : '+accessToken);
        return accessToken;
    }

    /*
    @Description: Generic method for callout with required parameters
	@Author: Maulik Patel
	*/
    public static HttpResponse doHttpCallout( String endPoint, String method,String ContentType, String accessToken, String body){
        
        Http httpWebRequest = new Http();
        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        request.setEndpoint(endPoint);
        request.setMethod(method);

        request.setHeader('Content-Type', ContentType);
        request.setHeader('Authorization', 'bearer '+accessToken);
        
        if(method == 'POST' || method == 'PUT'){
            // Set the body as a JSON object
            request.setBody(body);
        }

        try {
            response = httpWebRequest.send(request);
        } catch (Exception e) {
            System.debug('Exception while calling the call:'+e.getStackTraceString());
        } 
        
        return response;
    }

    /*
    @Description: Fpr creating body for creating docusign envelope
	@Author: Maulik Patel
	*/
    public static String JSONBody(List<MP_DocuSignControllerHelper.AttachmentWrapper> attachmentDetails,String email)
    {
        Integer i=1;
        JSONGenerator genJson = JSON.createGenerator(true);
        genJson.writeStartObject();
            genJson.writeFieldName('documents');
                genJson.writeStartArray();
                for (MP_DocuSignControllerHelper.AttachmentWrapper aw : attachmentDetails) {
                    genJson.writeStartObject();
                        genJson.writeStringField('documentBase64', aw.Body);
                        genJson.writeStringField('documentId', String.valueOf(i++));
                        genJson.writeStringField('fileExtension', aw.Extension);
                        genJson.writeStringField('name', aw.Name);
                    genJson.writeEndObject();
                }
                genJson.writeEndArray();
            genJson.writeStringField('emailSubject', 'Maulik-Email');
            genJson.writeFieldName('recipients');
                genJson.writeStartObject();
                genJson.writeFieldName('signers');
                    genJson.writeStartArray();
                        genJson.writeStartObject();
                            genJson.writeStringField('email', email);
                            genJson.writeStringField('name', 'Maulik');
                            genJson.writeStringField('recipientId', '1');
                            genJson.writeStringField('routingOrder', '1');
                        genJson.writeEndObject();
                    genJson.writeEndArray();
                genJson.writeEndObject();
            genJson.writeStringField('status', 'sent');
            genJson.writeEndObject();
        String jsonData = genJson.getAsString();
        System.debug('MP** GeneratedJson : '+jsonData);	
        return jsonData;
    }


    public class AccessTokenWrapper
    {
        public string access_token;
        public string token_type;
        public string scope; 
    }
    public class DocuSignEnvelopeResponseWrapper
    {
        public string envelopeId;
        public string uri;
        public string status;
        public Datetime statusDateTime;
    } 
   
}
