/*************************************************************
@Name: MP_CheckDocuSignEnvelopeStatusController
@Author: Maulik Patel
@Description: Controller class for VF Page MP_CheckDocuSignEnvelopeStatus.
*******************************************************************/

public with sharing class MP_CheckDocuSignEnvelopeStatusController {
    private static String docuSignAccountNumber;
    private static String docuSignAppId;

    public static Pagereference docuSignEnvelopeStatus(){
        String propertyId = Apexpages.currentPage().getParameters().get('Id');
        System.debug('Maulik inside propertyID : '+propertyId);
        
        // Invoking the Queuable class
        Id jobId = System.enqueueJob(new MP_CheckDocuSignEnvelopeStatusHelper(propertyId));
        System.debug('Queuable Job Id : '+ jobId);

        //Redirection Logic
        Pagereference pageref = new Pagereference(System.Url.getSalesforceBaseURL().toExternalForm() + '/'+propertyId);
        System.debug(pageref);
        pageref.setredirect(true);
        return pageref;
    }

}
