/************************************************************************************************************************************************
@Name: MP_CheckDocuSignEnvelopeStatusController
@Author: Maulik Patel
@Description: Helper class for MP_CheckDocuSignEnvelopeStatusController.
              Checks the status of docusign envelope and fetchs the document if status updated to completed and fetch and attach signed files to object
*************************************************************************************************************************************************/

global with sharing class MP_CheckDocuSignEnvelopeStatusHelper implements Queueable,Database.AllowsCallouts{
    private static String docuSignAccountNumber;
    private static String docuSignAppId;

    String propertyId;
    global MP_CheckDocuSignEnvelopeStatusHelper(String propertyID) {
        this.propertyId = propertyID;
    }

    global void execute(QueueableContext context)
    {
        System.debug('MP** Inside the Queueable class');
        String accessToken;
        List<String> docuSignEnvelopeIdList = new List<String>();
        Map<String,String> existingEnvelopeIdStatusMap = new Map<String,String>();
        Map<String,String> envelopeIdObjectIdMap = new Map<String,String>();
        List<DocuSign_Envelope__c>  envelopedetailList = [select Id, Envelope_ID__c,Status__c from DocuSign_Envelope__c 
                                                        where Property__c =: propertyId ];

        for(DocuSign_Envelope__c envelopeDetails : envelopedetailList)
        {
            if(envelopeDetails.Envelope_ID__c != null)
            {
                docuSignEnvelopeIdList.add(envelopeDetails.Envelope_ID__c);
                existingEnvelopeIdStatusMap.put(envelopeDetails.Envelope_ID__c, envelopeDetails.Status__c);
                envelopeIdObjectIdMap.put(envelopeDetails.Envelope_ID__c, envelopeDetails.Id);
            }
        }

        Docusign_Setting__mdt DocuSignAccountDetails = [select DocuSign_Account_Number__c
                                                        ,DocuSign_Application_ID__c 
                                                        from DocuSign_Setting__mdt where label = 'DocuSign Details' LIMIT 1];                  
        docuSignAccountNumber = DocuSignAccountDetails.DocuSign_Account_Number__c;
        docuSignAppId = DocuSignAccountDetails.DocuSign_Application_ID__c;
        accessToken =  MP_DocuSignEnvelopeCallout.getAccessToken(docuSignAppId);
        getEnvelopeStatus(accessToken,docuSignAccountNumber,docuSignEnvelopeIdList,existingEnvelopeIdStatusMap,envelopeIdObjectIdMap,propertyId);
    }

    public static void getEnvelopeStatus(String accessToken,String docuSignAccountNumber,List<String> docuSignEnvelopeIdList,Map<String,String> existingEnvelopeIdStatusMap,Map<String,String> envelopeIdObjectIdMap,String propertyId)
    {    
        
        HttpResponse response = new HttpResponse();
        HttpResponse documentresponse = new HttpResponse();
        String endpoint;
        String body;
        envelopeStatusResponseWrapper envelopeStatus;
        Map<String,String> latestEnvelopeIdStatusMap = new Map<String,String>();
        List<DocuSign_Envelope__c> updateDoucSignEnvelopeList = new List<DocuSign_Envelope__c>();
        List<String> completedStatus = new List<String>();

        String resultSetSize,totalSetSize;
        List<envelopeResponse> envelopeDetailsList = new List<envelopeResponse>();

        //For fetching the envelope status
        endPoint = 'callout:MP_DocuSignCredentials/accounts/'+docuSignAccountNumber+'/envelopes/status?envelope_ids=request_body';
        body = JSONBody(docuSignEnvelopeIdList );
        response = MP_DocuSignEnvelopeCallout.doHttpCallout(endPoint, 'PUT', 'application/json', accessToken, body); 
        envelopeStatus = (envelopeStatusResponseWrapper)JSON.deserialize(response.getBody(), envelopeStatusResponseWrapper.class);
        System.debug('MP** Deserialised envelopeStatus response'+envelopeStatus);
        resultSetSize = envelopeStatus.resultSetSize;
        totalSetSize = envelopeStatus.totalSetSize;
        envelopeDetailsList = envelopeStatus.envelopes;
        // System.debug('Maulik resultSetSize : '+resultSetSize+'totalSetSize : '+totalSetSize+' envelopeDetailsList : '+ envelopeDetailsList);
        for (envelopeResponse envelopeDetails : envelopeDetailsList) {
            // System.debug('Maulik Envelope Status : '+envelopeDetails.status );
            // System.debug('Maulik Envelope Status : '+envelopeDetails.envelopeId );
            latestEnvelopeIdStatusMap.put(envelopeDetails.envelopeId , envelopeDetails.status);
        }

        for(String envelopeId : latestEnvelopeIdStatusMap.keySet())
        {
            if(existingEnvelopeIdStatusMap.get(envelopeId) != latestEnvelopeIdStatusMap.get(envelopeId))
            {
                DocuSign_Envelope__c de = new DocuSign_Envelope__c();
                de.Id = envelopeIdObjectIdMap.get(envelopeId);
                de.Status__c = latestEnvelopeIdStatusMap.get(envelopeId);
                updateDoucSignEnvelopeList.add(de);
                if(latestEnvelopeIdStatusMap.get(envelopeId) == 'completed')
                {
                    completedStatus.add(envelopeId);
                }
            }
        }


        // For getting the document is there is any new status updated to completed 
        Map<String,Blob> blobMap = new Map<String,Blob>();
        if(!completedStatus.isEmpty())
        {
            for (String envID : completedStatus) 
            {
                endPoint = 'callout:MP_DocuSignCredentials/accounts/'+docuSignAccountNumber+'/envelopes/'+envID+'/documents/combined';
                documentresponse = MP_DocuSignEnvelopeCallout.doHttpCallout(endPoint, 'GET', 'application/json', accessToken, null);
                if(documentresponse.getStatusCode() == 200)
                {
                    System.debug('Maulik Inside the getting document');
                    blobMap.put(envID,documentresponse.getBodyAsBlob());
                }
            }
        }

        //Update the Docu Sign Envelope status
        if (!updateDoucSignEnvelopeList.isEmpty())
        {
            try {
                Update updateDoucSignEnvelopeList;
            } catch (Exception e) {
                System.debug('Unable to update the DocuSign_Envelope__c records'+e.getStackTraceString());
            }  
        }

        //Create ContenetVersion
        List<ContentVersion> cvList = new List<ContentVersion>();
        for (String envId : blobMap.keySet()) 
        {
            System.debug('Maulik Inside the ContenetVersion for');
            if(blobMap.get(envId) != null)
            {
                System.debug('Maulik Inside the ContenetVersion if');
                ContentVersion cv = new ContentVersion();
                cv.Title = 'Signed_'+String.valueOf(System.today())+envId;
                cv.PathOnClient = 'Signed_'+String.valueOf(System.today())+'-'+envId+'.pdf';
                cv.VersionData = blobMap.get(envId);
                cv.IsMajorVersion = true;
                cvList.add(cv);
            }   
        }
        Database.SaveResult[] cvInsertResult = Database.insert(cvList,false);
        List<Id> cvIdList= new List<Id>();
        for(Database.SaveResult sr : cvInsertResult)
        {
            if (sr.isSuccess()) 
            {
                cvIdList.add(sr.getId());
            }
        }

        // Create ContentdocumentLink to link with the property object
        List<ContentVersion> contentDocumentIdList = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id IN:cvIdList];
        List<ContentDocumentLink> cdLinkList = new List<ContentDocumentLink>();
        if (!cvIdList.isEmpty()) 
        {
            for (ContentVersion cvdocId : contentDocumentIdList) 
            {
                System.debug('Maulik Inside the ContenetLink if');
                ContentDocumentLink linkedDocument = new ContentDocumentLink();
                linkedDocument.ContentDocumentId = cvdocId.ContentDocumentId;
                linkedDocument.LinkedEntityId = propertyId;
                linkedDocument.ShareType = 'V';
                cdLinkList.add(linkedDocument);
            }
        }
        try {
            if (!cdLinkList.isEmpty()) 
            {
                insert cdLinkList;
            } 
        } catch (Exception e) {
            System.debug('Unable to inset the Link List');
        }

    }

    // To create JSON Body for request body
    public static String JSONBody(List<String> docuSignEnvelopeIdList )
    {
        JSONGenerator genJson = JSON.createGenerator(true);
        genJson.writeStartObject();
            genJson.writeFieldName('envelopeIds');
            genJson.writeStartArray();
                for (String docuSignEnvelopeId : docuSignEnvelopeIdList) {
                    genJson.writeString(docuSignEnvelopeId);
                }
            genJson.writeEndArray();
        genJson.writeEndObject();
        String jsonData = genJson.getAsString();
        // System.debug('GeneratedJson : '+jsonData);	
        return jsonData;
    }

    public class envelopeStatusResponseWrapper{
        String resultSetSize;
        String totalSetSize;
        List<envelopeResponse> envelopes;  
    }

    public class envelopeResponse{
        String status;
        String envelopeId;
    }

}