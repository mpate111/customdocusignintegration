public with sharing class MP_DocumnetUploadController {
    
    @Auraenabled
	public static ServerResponseMessage processDocument(Id PropertyId, Id documentId, String fileName, String startdate) {
        
        Savepoint sp = Database.setSavepoint();
        
        //Initializing the wrapper message 
        ServerResponseMessage returnMessage = new ServerResponseMessage();
        
        try{   
            Property__c property = new Property__c(Id=PropertyId);
            property.Contract_Start_Date__c = Date.valueOf(startdate);
            update property;
			
            
            // Create Activity record for tracking document upload History
            Task t = new Task();
            t.Subject = 'Upload Document Completed';
            t.Status = 'Completed';
            t.ActivityDate = System.today();
            t.Description = 'Document(s) imported: \n ------------------------------ \n'+FileName;
            t.WhatId = PropertyId;
            insert t;

            returnMessage.isSuccess = true;                                       
        	
        }Catch(Exception ex){

			System.debug( 'MP** Exception while uploading Document'+ ex.getMessage()); 
            returnMessage.isSuccess = false;            
            //Adding the error message if an exception is occured
            returnMessage.errorMsg = ex.getMessage();
            Database.rollback(sp);
            // delete uploaded file for unsuccessfull document upload
            ContentDocument cd = new ContentDocument(Id = documentId);
            try {
                delete cd;
            } catch (DmlException e) {
                System.debug('Unable to delete the uploaded file from Fully Signed Component :'+e.getStackTraceString());
            }
        }
        return returnMessage;
    }
    
    public class ServerResponseMessage { 
        
        @AuraEnabled public Boolean isSuccess;
        @AuraEnabled public String errorMsg; //error msg
        @AuraEnabled public String successMsg; //success msg
        
        public ServerResponseMessage(){
            isSuccess = true;
            errorMsg = '';
            successMsg = '';
        }
    }
}
