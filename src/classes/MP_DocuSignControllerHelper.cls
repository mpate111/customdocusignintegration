/***************************************************************************************************************
@Name: MP_DocuSignControllerHelper
@Author: Maulik Patel
@Description: Helper class for MP_DocuSignController.
              Fetches all the object documents in files and retuns to the controller
              Call the Queuable class for creating docusign envelope and record in DocusignEnvelope object
***************************************************************************************************************/

public with sharing class MP_DocuSignControllerHelper {

    @AuraEnabled
    public static List<AttachmentWrapper> loadAttachments(Id objectId){
    List<AttachmentWrapper> attList = new List<AttachmentWrapper>();
    Set<Id> conDocIdSet = new Set<Id>();
    Map<Id, AttachmentWrapper> conDocIdToConDocLinkMap = new Map<Id, AttachmentWrapper>();

    List<ContentDocumentLink> cdlList = [SELECT Id,LinkedEntityId, LinkedEntity.Name, ContentDocument.CreatedDate,
                                        ContentDocument.FileExtension, ContentDocument.Title , ContentDocumentId
                                        FROM ContentDocumentLink where LinkedEntityId =: objectId];
    System.debug('Maulik'+cdlList);
    for(ContentDocumentLink a : cdlList)
    {
      conDocIdSet.add(a.ContentDocumentId);
      AttachmentWrapper aw = new AttachmentWrapper();
      aw.Id = a.Id;
      aw.Name = a.ContentDocument.Title;
      aw.Extension = a.ContentDocument.FileExtension;
      aw.CreatedDate = a.ContentDocument.CreatedDate;
      // aw.Body = EncodingUtil.base64Encode(a.Body);
      aw.Body = '';

      conDocIdToConDocLinkMap.put(a.ContentDocumentId, aw);
    }

    List<ContentVersion> contentVersionList = [SELECT VersionData, ContentDocumentId FROM ContentVersion WHERE IsLatest = true AND ContentDocumentId IN: conDocIdSet];

    for(ContentVersion cv :contentVersionList )
    {
      if (conDocIdToConDocLinkMap.get(cv.ContentDocumentId) != null)
      {
        AttachmentWrapper aw = conDocIdToConDocLinkMap.get(cv.ContentDocumentId);
        aw.Body = EncodingUtil.base64Encode(cv.VersionData);
        attList.add(aw);
      }
    }
    return attList;
  }


  @AuraEnabled
  public static Boolean docuSignCallout(List<AttachmentWrapper> selectedFiles,String propertyId){
    System.debug('PropertyId : '+propertyId);
    Id jobId;
    jobId = System.enqueueJob(new MP_DocuSignEnvelopeCallout(selectedFiles,propertyId));
    System.debug('Maulik Queuable Job Id : '+jobId);
    if (jobId == null) 
    {
        return false;    
    }
    return true;
  }

    public class AttachmentWrapper
    {
        @AuraEnabled public String Id {get; set;}
        @AuraEnabled public String Name {get; set;}
        @AuraEnabled public String Extension {get; set;}
        @AuraEnabled public Datetime CreatedDate {get; set;}
        @AuraEnabled public String Body {get; set;}
    }
}
